import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor

from data_helper import PLOT_EXECUTION_TIME_LABEL_S, PLOT_FORESTS_LABEL
from data_helper import PLOT_MEAN_ERROR_LABEL_M
from data_helper import MIN_FORESTS, MAX_FORESTS, FORESTS_STEP, FORESTS
from data_helper import RESULTS_LOCATION, COMBINED_DATASET_LOCATION
from data_helper import COMBINED_INTERPOLATED_DATASET_LOCATION
from data_helper import HISTOGRAM_BINS, HISTOGRAM_X_LABEL, HISTOGRAM_Y_LABEL
from data_helper import TIMING_REPETITIONS, TIMES_LOCATION
from common import load_data, timeit

# Set Matplotlib options
matplotlib.use('Agg')
plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams["font.family"] = "Times New Roman"

# Data loading non-interpolated data
X_train, X_test, y_train, y_test = load_data(COMBINED_DATASET_LOCATION, 0.3)

error_mean = []
error_median = []
times = []

# Calculate the Random forests error
for i in range(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP):
    random_forests = RandomForestRegressor(n_estimators=i, verbose=False)
    random_forests.fit(X_train, y_train)
    pred, time = timeit(random_forests.predict, X_test, repeat=TIMING_REPETITIONS)
    distances = np.linalg.norm(pred - y_test, axis=1)
    error_mean.append(np.mean(distances))
    error_median.append(np.median(distances))
    times.append(time)

    if i == FORESTS:
        # Create histogram
        fig, axes1 = plt.subplots(1, 2, figsize=(8, 4))
        axes1[0].hist(distances, HISTOGRAM_BINS, density=True)
        axes1[0].set_title('Histogram of predicted distances by RF\n(Forests = {})'.format(FORESTS))
        axes1[0].set_xlabel(HISTOGRAM_X_LABEL)
        axes1[0].set_ylabel(HISTOGRAM_Y_LABEL)
        axes1[0].set_xlim([0, 10])
        axes1[0].set_xticks(np.arange(0, 11, 1))
        axes1[0].grid(True)

        # Print statistics
        print('Statistics RF (Forests={}):'.format(FORESTS))
        distances_pd = pd.Series(distances)
        print(distances_pd.describe())

        print('RF Mean error: {:.2f}m'.format(np.mean(distances)))
        print('RF Median error: {:.2f}m'.format(np.median(distances)))

# Plot error on forests
fig2, axes2 = plt.subplots(1, 2, figsize=(8, 4))
axes2[0].plot(range(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP), error_mean, marker='x', markersize=5, label='Mean error')
axes2[0].plot(range(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP), error_median, marker='x', markersize=5, label='Median error')
axes2[0].legend()
axes2[0].set_title('Error Rate Forests Value')
axes2[0].set_xlabel(PLOT_FORESTS_LABEL)
axes2[0].set_ylabel(PLOT_MEAN_ERROR_LABEL_M)
axes2[0].set_xlim([MIN_FORESTS, MAX_FORESTS])
axes2[0].set_xticks(np.arange(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP * 2))
axes2[0].grid(True)

# Data loading interpolated data
X_train_i, X_test_i, y_train_i, y_test_i = load_data(COMBINED_INTERPOLATED_DATASET_LOCATION, 0.3)

error_mean_i = []
error_median_i = []
times_i = []

# Calculate the Random forests error for interpolated data
for i in range(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP):
    random_forests_i = RandomForestRegressor(n_estimators=i, verbose=False)
    random_forests_i.fit(X_train_i, y_train_i)
    pred_i, time_i = timeit(random_forests_i.predict, X_test_i, repeat=TIMING_REPETITIONS)
    distances_i = np.linalg.norm(pred_i - y_test_i, axis=1)
    error_mean_i.append(np.mean(distances_i))
    error_median_i.append(np.median(distances_i))
    times_i.append(time_i)

    if i == FORESTS:
        # Create histogram
        axes1[1].hist(distances_i, HISTOGRAM_BINS, density=True)
        axes1[1].set_title('Histogram of predicted distances by RF:\nInterpolated data (Forests = {})'.format(FORESTS))
        axes1[1].set_xlabel(HISTOGRAM_X_LABEL)
        axes1[1].set_ylabel(HISTOGRAM_Y_LABEL)
        axes1[1].set_xlim([0, 10])
        axes1[1].set_xticks(np.arange(0, 11, 1))
        axes1[1].grid(True)
        fig.savefig(os.path.join(RESULTS_LOCATION, 'RF_histogram.svg'), format='svg')

        # Print statistics
        print('\nStatistics RF (Interpolated data, Forests={}):'.format(FORESTS))
        distances_i_pd = pd.Series(distances_i)
        print(distances_i_pd.describe())

        print('RF Mean error (interpolated data): {:.2f}m'.format(np.mean(distances_i)))
        print('RF Median error (interpolated data): {:.2f}m'.format(np.median(distances_i)))

# # Plot error on the amount of forests
axes2[1].plot(range(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP), error_mean_i, marker='x', markersize=5, label='Mean error')
axes2[1].plot(range(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP), error_median_i, marker='x', markersize=5, label='Median error')
axes2[1].legend()
axes2[1].set_title('Error Rate Forests Value\nInterpolated data')
axes2[1].set_xlabel(PLOT_FORESTS_LABEL)
axes2[1].set_ylabel(PLOT_MEAN_ERROR_LABEL_M)
axes2[1].set_xlim([MIN_FORESTS, MAX_FORESTS])
axes2[1].set_xticks(np.arange(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP * 2))
axes2[1].grid(True)
fig2.savefig(os.path.join(RESULTS_LOCATION, 'RF_Error_dependency_on_forests.svg'), format='svg')

# Plot execution times
fig3, axes3 = plt.subplots(figsize=(8, 4))
axes3.plot(range(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP), np.mean(times, axis=1), marker='x', markersize=5, label='Measured data')
axes3.plot(range(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP), np.mean(times_i, axis=1), marker='x', markersize=5, label='Interpolated data')
axes3.legend()
axes3.set_title('Number of Forests influencing Execution Time\n(Average of {} runs)'.format(TIMING_REPETITIONS))
axes3.set_xlabel(PLOT_FORESTS_LABEL)
axes3.set_ylabel(PLOT_EXECUTION_TIME_LABEL_S)
axes3.set_xlim([MIN_FORESTS - FORESTS_STEP, MAX_FORESTS + FORESTS_STEP])
axes3.set_xticks(np.arange(MIN_FORESTS - FORESTS_STEP, MAX_FORESTS + FORESTS_STEP + 1, FORESTS_STEP))
axes3.grid(True)
fig3.savefig(os.path.join(RESULTS_LOCATION, 'RF_times.svg'), format='svg')

# Save time data
os.makedirs(TIMES_LOCATION, exist_ok=True)

np.savetxt(os.path.join(TIMES_LOCATION, 'RF.csv'),
           np.array(times).T, delimiter=',', 
           header=','.join([str(i) for i in range(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP)]))
np.savetxt(os.path.join(TIMES_LOCATION, 'RF_i.csv'),
           np.array(times_i).T, delimiter=',', 
           header=','.join([str(i) for i in range(MIN_FORESTS, MAX_FORESTS + 1, FORESTS_STEP)]))