from collections import defaultdict
import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.neighbors import KNeighborsRegressor

from data_helper import PLOT_EXECUTION_TIME_LABEL_S, PLOT_K_LABEL
from data_helper import MAX_K, K, PLOT_MEAN_ERROR_LABEL_M
from data_helper import RESULTS_LOCATION, COMBINED_DATASET_LOCATION
from data_helper import COMBINED_INTERPOLATED_DATASET_LOCATION
from data_helper import HISTOGRAM_BINS, HISTOGRAM_X_LABEL, HISTOGRAM_Y_LABEL
from data_helper import TIMING_REPETITIONS, TIMES_LOCATION, FIELD_NAMES
from common import load_data, timeit, timeit_dataframe, get_gaussian_regression_data
from common import get_per_point_gaussian_regression_data

plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams["font.family"] = "Times New Roman"

rows = 2
columns = 5

#
# Create all figures
#
fig1, axes1 = plt.subplots(rows, columns, figsize=(columns * 4, rows * 4), constrained_layout=True, sharex=True, sharey=True)
fig2, axes2 = plt.subplots(rows, columns, figsize=(columns * 4, rows * 4), constrained_layout=True, sharex=True, sharey=True)
fig3, axes3 = plt.subplots(rows, columns, figsize=(columns * 4, rows * 4), constrained_layout=True, sharex=True, sharey=True)
fig4, axes4 = plt.subplots(rows, columns, figsize=(columns * 4, rows * 4), constrained_layout=True, sharex=True, sharey=True)

#
# Create all configurations
#
data_formats = ('measured', 'interpolated', 'gaus_1p_1m', 'gaus_1p_05m', 'gaus_50p_1m', 'gaus_50p_05m', 'meas_first', 'meas_mean', 'meas_median')
algorithms = ['brute'] #, 'ball_tree', 'kd_tree')

#
# Create output data storing dictionaries
#
error_mean = defaultdict(None)
error_median = defaultdict(None)
times_single = defaultdict(None)
times_group = defaultdict(None)
distances = defaultdict(None)

#
# Data loading
#
X_train = defaultdict(None)
X_test = defaultdict(None)
y_train = defaultdict(None)
y_test = defaultdict(None)

# Create Mesh  grids for 0.5m and 1m data grid
grid_step_05 = 0.5
x_distances_05 = np.arange(-0.15 + grid_step_05, 17.71 - grid_step_05, grid_step_05)
y_distances_05 = np.arange(-0.9 + grid_step_05, 11.76 - grid_step_05, grid_step_05)
X0_5, Y0_5 = np.meshgrid(x_distances_05, y_distances_05)

grid_step_1 = 1
x_distances_1 = np.arange(-0.15 + grid_step_1, 17.71 - grid_step_1, grid_step_1)
y_distances_1 = np.arange(-0.9 + grid_step_1, 11.76 - grid_step_1, grid_step_1)
X1, Y1 = np.meshgrid(x_distances_1, y_distances_1)

# Data loading non-interpolated data
X_train['measured'], X_test['measured'], y_train['measured'], y_test['measured'] = load_data(COMBINED_DATASET_LOCATION, 0.3)

# Data loading interpolated data
interpolated = load_data(COMBINED_INTERPOLATED_DATASET_LOCATION, split=False)
test_df = pd.concat([interpolated, pd.concat([y_test['measured'], X_test['measured']], axis=1),
                     pd.concat([y_test['measured'], X_test['measured']], axis=1)],
                    axis=0).drop_duplicates(keep=False)
X_train['interpolated'] = test_df[['RSSI_1', 'RSSI_2', 'RSSI_3', 'RSSI_4', 'RSSI_5']]
y_train['interpolated'] = test_df[['X', 'Y']]


# Get Gaussian regression data points for 0.5m grid, 1 point per location
X_train['gaus_1p_05m'], y_train['gaus_1p_05m'] = get_gaussian_regression_data(COMBINED_DATASET_LOCATION,
                                                                             x_distances_05,
                                                                             y_distances_05,
                                                                             radiomap=False)

# Get Gaussian regression data points for 0.5m grid, 50 points per location
X_train['gaus_50p_05m'], y_train['gaus_50p_05m'] = get_per_point_gaussian_regression_data(COMBINED_DATASET_LOCATION,
                                                                                         x_distances_05,
                                                                                         y_distances_05,
                                                                                         np_radiomap=False)

# Get Gaussian regression data points for 1m grid, 1 point per location
X_train['gaus_1p_1m'], y_train['gaus_1p_1m'] = get_gaussian_regression_data(COMBINED_DATASET_LOCATION,
                                                                           x_distances_1,
                                                                           y_distances_1,
                                                                           radiomap=False)

# Get Gaussian regression data points for 1m grid, 50 points per location
X_train['gaus_50p_1m'], y_train['gaus_50p_1m'] = get_per_point_gaussian_regression_data(COMBINED_DATASET_LOCATION,
                                                                                       x_distances_1,
                                                                                       y_distances_1,
                                                                                       np_radiomap=False)

# Get mean of measured data
data = load_data(COMBINED_DATASET_LOCATION, split=False)
mean_list = []
for x in np.arange(0.85, 17, 1):
    for y in np.arange(0.1, 11, 1):
        row = np.mean(data[(data['X'] == x) & (data['Y'] == y)], axis=0).values
        if not np.isnan(row).any():
            mean_list.append(row)

mean = pd.DataFrame(mean_list, columns=FIELD_NAMES)
X_train['meas_mean'] = mean[['RSSI_1', 'RSSI_2', 'RSSI_3', 'RSSI_4', 'RSSI_5']]
y_train['meas_mean'] = mean[['X', 'Y']]

data = load_data(COMBINED_DATASET_LOCATION, split=False)
median_list = []
for x in np.arange(0.85, 17, 1):
    for y in np.arange(0.1, 11, 1):
        row = np.median(data[(data['X'] == x) & (data['Y'] == y)], axis=0)
        if not np.isnan(row).any():
            median_list.append(row)

median = pd.DataFrame(median_list, columns=FIELD_NAMES)
X_train['meas_median'] = median[['RSSI_1', 'RSSI_2', 'RSSI_3', 'RSSI_4', 'RSSI_5']]
y_train['meas_median'] = median[['X', 'Y']]

# Get !st sample of measured data
loc = np.full((1, 50), 0)
for i in range(1, 142):
    loc = np.hstack((loc, np.full((1, 50), i)))

data['LOC'] = loc.T
firsts = pd.DataFrame()

test = pd.DataFrame(np.hstack((y_test['measured'].values, X_test['measured'].values)), columns=FIELD_NAMES)

for i in np.arange(0, 142):
    for idx in range(0, 50):
        index = i * 50 + idx
        rssi_1 = data['RSSI_1'][index:index+1].values[0]
        rssi_2 = data['RSSI_2'][index:index+1].values[0]
        rssi_3 = data['RSSI_3'][index:index+1].values[0]
        rssi_4 = data['RSSI_4'][index:index+1].values[0]
        rssi_5 = data['RSSI_5'][index:index+1].values[0]

        val = X_test['measured'][(X_test['measured']['RSSI_1'] == rssi_1) &
                           (X_test['measured']['RSSI_2'] == rssi_2) &
                           (X_test['measured']['RSSI_3'] == rssi_3) &
                           (X_test['measured']['RSSI_4'] == rssi_4) &
                           (X_test['measured']['RSSI_5'] == rssi_5)]
        if not val.empty:
            continue
        else:
            firsts = firsts.append(data[index:index+1].drop(labels=['LOC'], axis=1).copy())
            break

X_train['meas_first'] = firsts[['RSSI_1', 'RSSI_2', 'RSSI_3', 'RSSI_4', 'RSSI_5']]
y_train['meas_first'] = firsts[['X', 'Y']]

# Copy test data to all data options
for data_format in data_formats:
    X_test[data_format] = X_test['measured']
    y_test[data_format] = y_test['measured']

#
# Run kNNs in all configurations
#
for data_format in data_formats:
    error_mean[data_format] = defaultdict(list)
    error_median[data_format] = defaultdict(list)
    times_single[data_format] = defaultdict(list)
    times_group[data_format] = defaultdict(list)
    distances[data_format] = defaultdict(list)

    for algorithm in algorithms:
        for k in range(1, MAX_K):
            print('Data format: {}, Algorithm: {}, K: {}           '.format(data_format, algorithm, k), end='\r')
            # Prepare kNN
            knn = KNeighborsRegressor(n_neighbors=k, algorithm=algorithm)
            knn.fit(X_train[data_format], y_train[data_format])

            # Run timed predictions
            pred, time_solo = timeit_dataframe(knn.predict, X_test[data_format])
            pred, time_group = timeit(knn.predict, X_test[data_format], repeat=TIMING_REPETITIONS)

            # Calculate distances
            dists = np.linalg.norm(pred - y_test[data_format].values, axis=1)

            # Calculate mean and median errors
            error_mean[data_format][algorithm].append(np.mean(dists))
            error_median[data_format][algorithm].append(np.median(dists))
            times_single[data_format][algorithm].append(np.array(time_solo))
            times_group[data_format][algorithm].append(np.array(time_group))
            distances[data_format][algorithm].append(dists)

#
# Chart titles
#             
hist_titles = {'measured': 'Histogram of predicted distances by kNN\n(k = {})'.format(K),
               'interpolated': 'Histogram of predicted distances by kNN\nInterpolated data (k = {})'.format(K),
               'gaus_1p_1m': 'Histogram of predicted distances by kNN\nGaussian regression data (1P/RL, k = {}, {}m grid)'.format(K, grid_step_1),
               'gaus_1p_05m': 'Histogram of predicted distances by kNN\nGaussian regression data (1P/RL, k = {}, {}m grid)'.format(K, grid_step_05),
               'gaus_50p_1m': 'Histogram of predicted distances by kNN\nGaussian regression data (50P/RL, k = {}, {}m grid)'.format(K, grid_step_1),
               'gaus_50p_05m': 'Histogram of predicted distances by kNN\nGaussian regression data (50P/RL, k = {}, {}m grid)'.format(K, grid_step_05),
               'meas_first': 'Histogram of predicted distances by kNN\nMeasured 1p data (k = {})'.format(K),
               'meas_mean': 'Histogram of predicted distances by kNN\nMeasured mean data (k = {})'.format(K),
               'meas_median': 'Histogram of predicted distances by kNN\nMeasured median data (k = {})'.format(K)}

error_titles = {'measured': 'Error Rate K Value - Measured data\nTrain/Test: {}/{}'.format(len(X_train['measured']), len(X_test['measured'])),
                'interpolated': 'Error Rate K Value - Interpolated data\nTrain/Test: {}/{}'.format(len(X_train['interpolated']), len(X_test['interpolated'])),
                'gaus_1p_1m': 'Error Rate K Value - Gaussian regression data\n(1P/RL, {}m grid, Train/Test: {}/{})'.format(grid_step_1, len(X_train['gaus_1p_1m']), len(X_test['gaus_1p_1m'])),
                'gaus_1p_05m': 'Error Rate K Value - Gaussian regression data\n(1P/RL, {}m grid, Train/Test: {}/{})'.format(grid_step_05, len(X_train['gaus_1p_05m']), len(X_test['gaus_1p_05m'])),
                'gaus_50p_1m': 'Error Rate K Value - Gaussian regression data\n(50P/RL, {}m grid, Train/Test: {}/{})'.format(grid_step_1, len(X_train['gaus_50p_1m']), len(X_test['gaus_50p_1m'])),
                'gaus_50p_05m': 'Error Rate K Value - Gaussian regression data\n(50P/RL, {}m grid, Train/Test: {}/{})'.format(grid_step_05, len(X_train['gaus_50p_05m']), len(X_test['gaus_50p_05m'])),
                'meas_first': 'Error Rate K Value - Measured 1p data\nTrain/Test: {}/{}'.format(len(X_train['meas_first']), len(X_test['meas_first'])),
                'meas_mean': 'Error Rate K Value - Measured mean data\nTrain/Test: {}/{}'.format(len(X_train['meas_mean']), len(X_test['meas_mean'])),
                'meas_median': 'Error Rate K Value - Measured median data\nTrain/Test: {}/{}'.format(len(X_train['meas_median']), len(X_test['meas_median']))}

exec_titles = {'measured': 'Execution speed - Measured data\nTrain/Test: {}/{}'.format(len(X_train['measured']), len(X_test['measured'])),
               'interpolated': 'Execution speed - Interpolated data\nTrain/Test: {}/{}'.format(len(X_train['interpolated']), len(X_test['interpolated'])),
               'gaus_1p_1m': 'Execution speed - Gaussian regression data\n(1P/RL, {}m grid, Train/Test: {}/{})'.format(grid_step_1, len(X_train['gaus_1p_1m']), len(X_test['gaus_1p_1m'])),
               'gaus_1p_05m': 'Execution speed - Gaussian regression data\n(1P/RL, {}m grid, Train/Test: {}/{})'.format(grid_step_05, len(X_train['gaus_1p_05m']), len(X_test['gaus_1p_05m'])),
               'gaus_50p_1m': 'Execution speed - Gaussian regression data\n(50P/RL, {}m grid, Train/Test: {}/{})'.format(grid_step_1, len(X_train['gaus_50p_1m']), len(X_test['gaus_50p_1m'])),
               'gaus_50p_05m': 'Execution speed - Gaussian regression data\n(50P/RL, {}m grid, Train/Test: {}/{})'.format(grid_step_05, len(X_train['gaus_50p_05m']), len(X_test['gaus_50p_05m'])),
               'meas_first': 'Execution speed - Measured 1p data\nTrain/Test: {}/{}'.format(len(X_train['meas_first']), len(X_test['meas_first'])),
               'meas_mean': 'Execution speed - Measured mean data\nTrain/Test: {}/{}'.format(len(X_train['meas_mean']), len(X_test['meas_mean'])),
               'meas_median': 'Execution speed - Measured median data\nTrain/Test: {}/{}'.format(len(X_train['meas_median']), len(X_test['meas_median']))}
#
# Plot all charts
#
for idx, data_format in enumerate(data_formats):
    row = int(idx / columns)
    column = int(idx % columns)
    
    # Set titles, limits etc for the plots
    axes1[row, column].set_title(hist_titles[data_format])
    axes2[row, column].set_title(error_titles[data_format])
    axes3[row, column].set_title(exec_titles[data_format])
    axes4[row, column].set_title(exec_titles[data_format])

    # Plot data
    axes1[row, column].hist(distances[data_format]['brute'][K-1], HISTOGRAM_BINS, density=True)
    
    for algorithm in algorithms:
        axes2[row, column].plot(range(1, MAX_K), error_mean[data_format][algorithm], marker='x', markersize=5, label='Mean error ({})'.format(algorithm))
        axes2[row, column].plot(range(1, MAX_K), error_median[data_format][algorithm], marker='x', markersize=5, label='Median error ({})'.format(algorithm))

        axes3[row, column].plot(range(1, MAX_K), np.mean(times_single[data_format][algorithm], axis=1), label='algorithm={}'.format(algorithm))

        axes4[row, column].plot(range(1, MAX_K), np.mean(times_group[data_format][algorithm], axis=1), label='algorithm={}'.format(algorithm))

    # Add axes labels
    for axes, xlabel, ylabel in zip([axes1, axes2, axes3, axes4],
                                    [HISTOGRAM_X_LABEL, 
                                     PLOT_K_LABEL,
                                     PLOT_K_LABEL,
                                     PLOT_K_LABEL],
                                    [HISTOGRAM_Y_LABEL,
                                     PLOT_MEAN_ERROR_LABEL_M,
                                     PLOT_EXECUTION_TIME_LABEL_S,
                                     PLOT_EXECUTION_TIME_LABEL_S]):
        if row == rows - 1:
            axes[row, column].set_xlabel(xlabel)
        if column == 0:
            axes[row, column].set_ylabel(ylabel)

    # Show Legends and grids and X limits
    for axes, xlim in zip([axes1, axes2, axes3, axes4],
                          [10, MAX_K, MAX_K, MAX_K]):
        if axes is not axes1:
            axes[row, column].legend()
        axes[row, column].grid(True)
        axes[row, column].set_xlim([0, xlim])

    axes1[row, column].set_xticks(np.arange(0, 11, 1))

#
# Save figures
#
fig1.savefig(os.path.join(RESULTS_LOCATION, 'kNN_histogram.svg'))
fig2.savefig(os.path.join(RESULTS_LOCATION, 'kNN_Error_dependency_on_K.svg'))
fig3.savefig(os.path.join(RESULTS_LOCATION, 'kNN_times_single.svg'))
fig4.savefig(os.path.join(RESULTS_LOCATION, 'kNN_times_group.svg'))

#
# save execution times
#
os.makedirs(TIMES_LOCATION, exist_ok=True)

for data_format in data_formats:
    for algorithm in algorithms:
        np.savetxt(os.path.join(TIMES_LOCATION, 'kNN_single_algorithm-{}_dataformat-{}.csv'.format(algorithm, data_format)),
                   np.array(times_single[data_format][algorithm]).T, delimiter=',', 
                   header=','.join([str(i) for i in range(1, MAX_K)]))
        np.savetxt(os.path.join(TIMES_LOCATION, 'kNN_group_algorithm-{}_dataformat-{}.csv'.format(algorithm, data_format)),
                   np.array(times_group[data_format][algorithm]).T, delimiter=',', 
                   header=','.join([str(i) for i in range(1, MAX_K)]))
