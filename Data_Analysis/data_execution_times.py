import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from data_helper import TIMES_LOCATION, PLOT_EXECUTION_TIME_LABEL_S
from data_helper import MAX_K, PLOT_K_LABEL, RESULTS_LOCATION
from data_helper import MIN_FORESTS, MAX_FORESTS, FORESTS_STEP, PLOT_FORESTS_LABEL

# Set Matplotlib options
matplotlib.use('Agg')
plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams["font.family"] = "Times New Roman"

# Load execution times for both measured and interpolated kNN data
knn = np.loadtxt(os.path.join(TIMES_LOCATION, 'kNN.csv'), delimiter=',',skiprows=1)
knn_err = np.array([np.mean(knn, axis=0) - np.min(knn, axis=0), np.max(knn, axis=0) - np.mean(knn, axis=0)])

knn_i = np.loadtxt(os.path.join(TIMES_LOCATION, 'kNN_i.csv'), delimiter=',',skiprows=1)
knn_i_err = np.array([np.mean(knn_i, axis=0) - np.min(knn_i, axis=0), np.max(knn_i, axis=0) - np.mean(knn_i, axis=0)])

# Create the plot
fig1, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(12, 5), constrained_layout=True) #
fig1.suptitle('Execution times comparison', fontsize=16)

# Plot kNN
ax1.errorbar(np.arange(start=1, stop=MAX_K, step=1), np.mean(knn, axis=0), yerr=knn_err, capsize=5, label='Measured kNN', color='tab:blue')
ax1.errorbar(np.arange(start=1, stop=MAX_K, step=1), np.mean(knn_i, axis=0), yerr=knn_i_err, capsize=5, label='Interpolated kNN', color='tab:orange')
ax1.set_title('kNN execution times based on value of K')
ax1.set_xlim([0, MAX_K])
ax1.set_xticks(np.arange(0, MAX_K + 1, 2))
ax1.set_xlabel(PLOT_K_LABEL)
ax1.set_ylabel(PLOT_EXECUTION_TIME_LABEL_S)
ax1.legend()
ax1.grid(True)

# Load execution times for both measured and interpolated Random Forests data
rf = np.loadtxt(os.path.join(TIMES_LOCATION, 'rf.csv'), delimiter=',',skiprows=1)
rf_err = np.array([np.mean(rf, axis=0) - np.min(rf, axis=0), np.max(rf, axis=0) - np.mean(rf, axis=0)])

rf_i = np.loadtxt(os.path.join(TIMES_LOCATION, 'rf_i.csv'), delimiter=',',skiprows=1)
rf_i_err = np.array([np.mean(rf_i, axis=0) - np.min(rf_i, axis=0), np.max(rf_i, axis=0) - np.mean(rf_i, axis=0)])

# Plot Random Forest
ax2.errorbar(np.arange(start=MIN_FORESTS, stop=MAX_FORESTS + 1, step=FORESTS_STEP), np.mean(rf, axis=0), yerr=rf_err, capsize=5, label='Measured RF', color='tab:green')
ax2.errorbar(np.arange(start=MIN_FORESTS, stop=MAX_FORESTS + 1, step=FORESTS_STEP), np.mean(rf_i, axis=0), yerr=rf_i_err, capsize=5, label='Interpolated RF', color='tab:red')
ax2.set_title('Random Forests execution times\nbased on number of forests')
ax2.set_xlim([MIN_FORESTS - FORESTS_STEP, MAX_FORESTS + FORESTS_STEP])
ax2.set_xticks(np.arange(MIN_FORESTS - FORESTS_STEP, MAX_FORESTS + FORESTS_STEP + 1, FORESTS_STEP * 2))
ax2.set_xlabel(PLOT_FORESTS_LABEL)
ax2.set_ylabel(PLOT_EXECUTION_TIME_LABEL_S)
ax2.legend()
ax2.grid(True)

# Load execution times for both measured and interpolated SVM data
svm = np.loadtxt(os.path.join(TIMES_LOCATION, 'svm.csv'), delimiter=',',skiprows=1)
svm_err = np.array([np.mean(svm, axis=0) - np.min(svm, axis=0), np.max(svm, axis=0) - np.mean(svm, axis=0)])

svm_i = np.loadtxt(os.path.join(TIMES_LOCATION, 'svm_i.csv'), delimiter=',',skiprows=1)
svm_i_err = np.array([np.mean(svm_i, axis=0) - np.min(svm_i, axis=0), np.max(svm_i, axis=0) - np.mean(svm_i, axis=0)])

# Get SVM labels
with open(os.path.join(TIMES_LOCATION, 'svm.csv')) as file:
    header = file.readline()
    labels = [''.join(i.split(';')[0:2]) for i in header.replace('# ', '').replace('\n', '').split(',')]

# Plot SVM
ax3.errorbar(np.arange(start=1, stop=13, step=1), np.mean(svm, axis=0), yerr=svm_err, capsize=5, label='Measured SVM', color='tab:purple')
ax3.errorbar(np.arange(start=1, stop=13, step=1), np.mean(svm_i, axis=0), yerr=svm_i_err, capsize=5, label='Interpolated SVM', color='tab:brown')
ax3.set_title('SVM execution times\nbased on configuration')
ax3.set_xlim([0, 13])
ax3.set_xticks(np.arange(1, 13, 1))
ax3.set_xticklabels(labels, rotation = 45, ha="right")
ax3.set_xlabel('SVM Configuration')
ax3.set_ylabel(PLOT_EXECUTION_TIME_LABEL_S)
ax3.legend()
ax3.grid(True)

# Save data
fig1.savefig(os.path.join(RESULTS_LOCATION, 'Execution_times_comparison.svg'), format='svg')
