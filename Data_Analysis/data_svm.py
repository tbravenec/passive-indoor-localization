import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.multioutput import MultiOutputRegressor
from sklearn.svm import SVR

from data_helper import RESULTS_LOCATION, COMBINED_DATASET_LOCATION
from data_helper import COMBINED_INTERPOLATED_DATASET_LOCATION
from data_helper import HISTOGRAM_BINS, HISTOGRAM_X_LABEL, HISTOGRAM_Y_LABEL
from data_helper import TIMING_REPETITIONS, TIMES_LOCATION
from data_helper import PLOT_EXECUTION_TIME_LABEL_S
from common import load_data, timeit

# Set Matplotlib options
matplotlib.use('Agg')
plt.rcParams['svg.fonttype'] = 'none'
plt.rcParams["font.family"] = "Times New Roman"

# Data loading non-interpolated data
X_train, X_test, y_train, y_test = load_data(COMBINED_DATASET_LOCATION, 0.3)

error_mean = []
error_median = []
times = []

# Calculate the SVM error
svm_model = SVR(verbose=False)
svm_mu = MultiOutputRegressor(svm_model)
svm_mu.fit(X_train, y_train)
pred, time = timeit(svm_mu.predict, X_test, repeat=TIMING_REPETITIONS)
distances = np.linalg.norm(pred - y_test, axis=1)
error_mean.append(np.mean(distances))
error_median.append(np.median(distances))
times.append(np.array(time))

# Print statistics
print('Statistics SVM:')
distances_pd = pd.Series(distances)
print(distances_pd.describe())

print('SVM Mean error: {:.2f}m'.format(np.mean(distances)))
print('SVM Median error: {:.2f}m'.format(np.median(distances)))

# Create histogram
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
ax1.hist(distances, HISTOGRAM_BINS, density=True)
ax1.set_title('Histogram of predicted distances by SVM')
ax1.set_xlabel(HISTOGRAM_X_LABEL)
ax1.set_ylabel(HISTOGRAM_Y_LABEL)
ax1.set_xlim([0, 10])
ax1.set_xticks(np.arange(0, 11, 1))
ax1.grid(True)

# Data loading interpolated data
X_train_i, X_test_i, y_train_i, y_test_i = load_data(COMBINED_INTERPOLATED_DATASET_LOCATION, 0.3)

error_mean_i = []
error_median_i = []
times_i = []

# Calculate the SVM error for interpolated data
svm_model_i = SVR()
svm_mu_i = MultiOutputRegressor(svm_model_i)
svm_mu_i.fit(X_train_i, y_train_i)
pred_i, time_i = timeit(svm_mu_i.predict, X_test_i, repeat=TIMING_REPETITIONS)
distances_i = np.linalg.norm(pred_i - y_test_i, axis=1)
error_mean_i.append(np.mean(distances_i))
error_median_i.append(np.median(distances_i))
times_i.append(np.array(time_i))

# Print statistics
print('\nStatistics SVM (Interpolated data):')
distances_i_pd = pd.Series(distances_i)
print(distances_i_pd.describe())

print('SVM Mean error (interpolated data): {:.2f}m'.format(np.mean(distances_i)))
print('SVM Median error (interpolated data): {:.2f}m'.format(np.median(distances_i)))

# Create histogram
ax2.hist(distances_i, HISTOGRAM_BINS, density=True)
ax2.set_title('Histogram of predicted distances by SVM:\nInterpolated data')
ax2.set_xlabel(HISTOGRAM_X_LABEL)
ax2.set_ylabel(HISTOGRAM_Y_LABEL)
ax2.set_xlim([0, 10])
ax2.set_xticks(np.arange(0, 11, 1))
ax2.grid(True)
fig.savefig(os.path.join(RESULTS_LOCATION, 'SVM_histogram.svg'), format='svg')

# Plot execution times
fig3, ax5 = plt.subplots(figsize=(8, 4))
ax5.bar(['Measured data'], np.mean(times, axis=1))
ax5.bar(['Interpolated data'], np.mean(times_i, axis=1))
ax5.set_title('SVM Execution Time\n(Average of {} runs)'.format(TIMING_REPETITIONS))
ax5.set_ylabel(PLOT_EXECUTION_TIME_LABEL_S)
ax5.grid(True)
fig3.savefig(os.path.join(RESULTS_LOCATION, 'SVM_times.svg'), format='svg')

# SVM parameter testing
C_range = [1e-2, 1, 1e2]
gamma_range = ['scale', 1e-3, 1e-2, 1e-1]
classifiers = []
times = []
descriptions = []

# Create SVM models
for C in C_range:
    for gamma in gamma_range:
        clf = SVR(C=C, gamma=gamma)
        clf_mu = MultiOutputRegressor(clf)
        clf_mu.fit(X_train, y_train)
        classifiers.append((C, gamma, clf_mu))

fig4, axes = plt.subplots(len(C_range), len(gamma_range), figsize=(16, 12), constrained_layout=True)
fig4.suptitle('SVM parameter comparison', fontsize=16)

# Plot all histograms
for idx, (C, gamma, clf) in enumerate(classifiers):
    pred, time = timeit(clf.predict, X_test, repeat=TIMING_REPETITIONS)
    distances = np.linalg.norm(pred - y_test, axis=1)
    times.append(time)

    axes[int(idx / len(gamma_range))][int(idx % len(gamma_range))].hist(distances, HISTOGRAM_BINS, density=True)

    if gamma == 'scale':
        descriptions.append('gamma={}; C=10^{}; Mean error={:.2f}m'.format(gamma, np.log10(C), np.mean(distances)))
        axes[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_title(descriptions[-1])
    else:
        descriptions.append('gamma=10^{}; C=10^{}; Mean error={:.2f}m'.format(np.log10(gamma), np.log10(C), np.mean(distances)))
        axes[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_title(descriptions[-1])
    axes[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_xlabel(HISTOGRAM_X_LABEL)
    axes[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_ylabel(HISTOGRAM_Y_LABEL)
    axes[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_xlim([0, 10])
    axes[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_xticks(np.arange(0, 11, 1))
    axes[int(idx / len(gamma_range))][int(idx % len(gamma_range))].grid(True)

fig4.savefig(os.path.join(RESULTS_LOCATION, 'SVM_Parameters.svg'), format='svg')

times_i = []
descriptions_i = []
classifiers_i = []

# Create SVM models for interpolated data
for C in C_range:
    for gamma in gamma_range:
        clf = SVR(C=C, gamma=gamma)
        clf_mu_i = MultiOutputRegressor(clf)
        clf_mu_i.fit(X_train_i, y_train_i)
        classifiers_i.append((C, gamma, clf_mu_i))

fig5, axes2 = plt.subplots(len(C_range), len(gamma_range), figsize=(16, 12), constrained_layout=True)
fig5.suptitle('SVM parameter comparison - interpolated data', fontsize=16)

# Plot all histograms
for idx, (C, gamma, clf) in enumerate(classifiers_i):
    pred_i, time_i = timeit(clf.predict, X_test_i, repeat=TIMING_REPETITIONS)
    distances_i = np.linalg.norm(pred_i - y_test_i, axis=1)
    times_i.append(time_i)

    axes2[int(idx / len(gamma_range))][int(idx % len(gamma_range))].hist(distances_i, HISTOGRAM_BINS, density=True)

    if gamma == 'scale':
        descriptions_i.append('gamma={}; C=10^{}; Mean error={:.2f}m'.format(gamma, np.log10(C), np.mean(distances_i)))
        axes2[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_title(descriptions_i[-1])
    else:
        descriptions_i.append('gamma=10^{}; C=10^{}; Mean error={:.2f}m'.format(np.log10(gamma), np.log10(C), np.mean(distances_i)))
        axes2[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_title(descriptions_i[-1])
    axes2[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_xlabel(HISTOGRAM_X_LABEL)
    axes2[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_ylabel(HISTOGRAM_Y_LABEL)
    axes2[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_xlim([0, 10])
    axes2[int(idx / len(gamma_range))][int(idx % len(gamma_range))].set_xticks(np.arange(0, 11, 1))
    axes2[int(idx / len(gamma_range))][int(idx % len(gamma_range))].grid(True)

fig5.savefig(os.path.join(RESULTS_LOCATION, 'SVM_Parameters_i.svg'), format='svg')

# Save time data
os.makedirs(TIMES_LOCATION, exist_ok=True)

np.savetxt(os.path.join(TIMES_LOCATION, 'SVM.csv'),
           np.array(times).T, delimiter=',', 
           header=','.join(descriptions))
np.savetxt(os.path.join(TIMES_LOCATION, 'SVM_i.csv'),
           np.array(times_i).T, delimiter=',', 
           header=','.join(descriptions_i))
